package au.edu.sydney.service;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.edu.sydney.dao.OrderDao;
import au.edu.sydney.dao.ProductAllDao;
import au.edu.sydney.domain.Orders;
import au.edu.sydney.domain.Product2;

@Service(value = "orderService")
public class OrderServiceImpl implements OrderService{

	@Autowired
	private OrderDao orderDao;
	
	@Autowired
	private ProductAllDao productDao;
	

	@Transactional
	public void addOrder() {
		Orders order=new Orders();
		//order.setOrder_id(1);
		order.setUser_id(1);
		order.setOrder_time(new Timestamp(System.currentTimeMillis()));
		order.setPick_time(new Timestamp(System.currentTimeMillis()));
		order.setOrder_status("paid");
		order.setTotal_price(30.00);
		order.setDescription("this is first order");
		order.setUser_visiable(0);
		System.out.println("this is now: "+order.getPick_time());
	//	List<Product2> p=productDao.getAllProduct();
		orderDao.saveOrder(order);
		System.out.println("add order successful!");
	}

	@Transactional
	public  Orders getOrder(int pnum){
	
		return orderDao.loadOrder(pnum);
		
}

	@Override
	public List<Orders> listOrder() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Orders newOrder() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getPickTime() {
		// TODO Auto-generated method stub
		return null;
	}

}
