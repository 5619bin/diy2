package au.edu.sydney.service;

import java.util.List;

import au.edu.sydney.domain.Orders;

public interface OrderService {
	
	public void addOrder();
	
	public List<Orders> listOrder();
	
	public Orders newOrder();
	
	public String getPickTime();
	
	
}
