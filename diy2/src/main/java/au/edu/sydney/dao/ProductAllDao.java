package au.edu.sydney.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import au.edu.sydney.domain.Orders;
import au.edu.sydney.domain.Product;
import au.edu.sydney.domain.Product2;

@Repository(value = "productAllDao")

public class ProductAllDao {
	 @Resource
	    private SessionFactory sessionFactory;

	    public SessionFactory getSessionFactory() {
	        return sessionFactory;
	    }

	    public void setSessionFactory(SessionFactory sessionFactory) {
	        this.sessionFactory = sessionFactory;
	    }

	    public void saveProduct(Product2 product) {
	        sessionFactory.getCurrentSession().save(product);
	    }
	    
	    public Product2 loadProduct(int pnum){
	        Product2 product = (Product2)sessionFactory.getCurrentSession().get(Product2.class, pnum);
	           return product;
	    }
	    
	    
	    @Transactional
	    public List<Product2> getAllProduct() {
	        
	    	// get the current hibernate session
	    	Session currentSession = sessionFactory.getCurrentSession();
	    
	    	// create a query
	    	Query theQuery = 
	    			currentSession.createQuery("from Product2");
	    	
	    	// execute query and get result list
	    	List<Product2> products= theQuery.list();
	    			
	    	// return the results		
	    	return products;
	    	
	    }
}
