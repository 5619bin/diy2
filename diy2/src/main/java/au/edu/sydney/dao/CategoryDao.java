package au.edu.sydney.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import au.edu.sydney.domain.Category;

@Repository(value = "categoryDao")

public class CategoryDao {
	@Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void saveCategory(Category category) {
        sessionFactory.getCurrentSession().save(category);
    }
    
    public Category loadCategory(int cnum){
    	Category category = (Category)sessionFactory.getCurrentSession().get(Category.class, cnum);
           return category;
    }
    
    @Transactional
    public Category deleteCategory(int cnum){
    	Category category = (Category)sessionFactory.getCurrentSession().get(Category.class, cnum);
        sessionFactory.getCurrentSession().delete(category);
           return category;
    }
    
    @Transactional
    public List<Category> listcategory() {
        
    	// get the current hibernate session
    	Session currentSession = sessionFactory.getCurrentSession();
    
    	// create a query
    	Query theQuery = 
    			currentSession.createQuery("from Category");
    	
    	// execute query and get result list
    	List<Category> categories= theQuery.list();
    			
    	// return the results		
    	return categories;    	
    }
    
    @Transactional
    public Boolean updateNamec(int c_num, String name){
    	Session currentSession = sessionFactory.getCurrentSession();
    	Category category = (Category)currentSession.get(Category.class, c_num);
    	category.setCname(name);
    	//currentSession.getTransaction().commit();
        return true;
    }
}
