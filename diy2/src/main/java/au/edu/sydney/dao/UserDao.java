package au.edu.sydney.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import au.edu.sydney.domain.Orders;
import au.edu.sydney.domain.User;

@Repository(value = "userDao")
public class UserDao {

    @Resource
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void saveUser(User user) {
        sessionFactory.getCurrentSession().save(user);
    }
    
    public List<User> getUsers() {
    	// get the current hibernate session
    	Session currentSession = sessionFactory.getCurrentSession();
    	// create a query
    	Query theQuery = currentSession.createQuery("from User");
    	// execute query and get result list
    	List<User> users= theQuery.list();		
    	// return the results		
    	return users;    	
    }
    
    @Transactional
    public Boolean updateEmail(int userid, String useremail){
    	Session currentSession = sessionFactory.getCurrentSession();
    	User user = (User)currentSession.get(User.class, userid);
    	user.setUseremail(useremail);
    //	currentSession.getTransaction().commit();
        return true;
    }
    
    @Transactional
    public Boolean updatePhone(int userid, String userphone){
    	Session currentSession = sessionFactory.getCurrentSession();
    	User user = (User)currentSession.get(User.class, userid);
    	user.setUserphone(userphone);
    //	currentSession.getTransaction().commit();
        return true;
    }
    
    @Transactional
    public Boolean updatePsw(int userid, String password){
    	Session currentSession = sessionFactory.getCurrentSession();
    	User user = (User)currentSession.get(User.class, userid);
    	user.setPassword(password);
    //	currentSession.getTransaction().commit();
        return true;
    }
}