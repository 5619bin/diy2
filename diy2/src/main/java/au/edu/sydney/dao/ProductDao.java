package au.edu.sydney.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import au.edu.sydney.domain.Orders;
import au.edu.sydney.domain.Product;

@Repository(value = "productDao")

public class ProductDao {
	 @Resource
	    private SessionFactory sessionFactory;

	    public SessionFactory getSessionFactory() {
	        return sessionFactory;
	    }

	    public void setSessionFactory(SessionFactory sessionFactory) {
	        this.sessionFactory = sessionFactory;
	    }
	    
	    @Transactional
	    public void saveProduct(Product product) {
	        sessionFactory.getCurrentSession().save(product);
	    }
	    
	    @Transactional
	    public Product loadProduct(int pnum){
	        Product product = (Product)sessionFactory.getCurrentSession().get(Product.class, pnum);
	           return product;
	    }
	    
	    @Transactional
	    public Product deleteProduct(int pnum){
	        Product product = (Product)sessionFactory.getCurrentSession().get(Product.class, pnum);
	        sessionFactory.getCurrentSession().delete(product);
	           return product;
	    }
	    
	    
	    @Transactional
	    public List<Product> listproduct() {
	        
	    	// get the current hibernate session
	    	Session currentSession = sessionFactory.getCurrentSession();
	    
	    	// create a query
	    	Query theQuery = 
	    			currentSession.createQuery("from Product");
	    	
	    	// execute query and get result list
	    	List<Product> product= theQuery.list();
	    			
	    	// return the results		
	    	return product;    	
	    }
	    @Transactional
	    public Boolean updateName(int p_num, String name){
	    	Session currentSession = sessionFactory.getCurrentSession();
	    	Product product = (Product)currentSession.get(Product.class, p_num);
	    	product.setPname(name);
	    	//currentSession.getTransaction().commit();
	        return true;
	    }
	    @Transactional
	    public Boolean updateCategory(int p_num, int p_cnum){
	    	Session currentSession = sessionFactory.getCurrentSession();
	    	Product product = (Product)currentSession.get(Product.class, p_num);
	    	product.setP_cnum(p_cnum);
	    	//currentSession.getTransaction().commit();
	        return true;
	    }
	    @Transactional
	    public Boolean updateQuantity(int p_num, int quantity){
	    	Session currentSession = sessionFactory.getCurrentSession();
	    	Product product = (Product)currentSession.get(Product.class, p_num);
	    	product.setQuantity(quantity);;
	    	//currentSession.getTransaction().commit();
	        return true;
	    }
	    @Transactional
	    public Boolean updatePrice(int p_num, double price){
	    	Session currentSession = sessionFactory.getCurrentSession();
	    	Product product = (Product)currentSession.get(Product.class, p_num);
	    	product.setPrice(price);
	    	//currentSession.getTransaction().commit();
	        return true;
	    }
	    @Transactional
	    public Boolean updateDescriptiony(int p_num, String description){
	    	Session currentSession = sessionFactory.getCurrentSession();
	    	Product product = (Product)currentSession.get(Product.class, p_num);
	    	product.setDescription(description);
	    	//currentSession.getTransaction().commit();
	        return true;
	    }
	    
}
