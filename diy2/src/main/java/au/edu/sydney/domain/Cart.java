package au.edu.sydney.domain;

public class Cart {

	private int No;
	private String name;
	private String description;
	private int quantity;
	private Double price;
	
	public Cart(){
		
	}
	
	public Cart(String name, String description, int quantity, Double price) {
		this.name = name;
		this.description = description;
		this.quantity = quantity;
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	
	public int getNo() {
		return No;
	}

	public void setNo(int no) {
		No = no;
	}

	@Override
	public String toString() {
		return "Cart [name=" + name + ", description=" + description + ", quantity=" + quantity + ", price=" + price
				+ "]";
	}
	
	public String toDes(){
		
		
		return null;
	}
	
}
