package au.edu.sydney.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class OrderToJson {

	private String order_id;
	
	private String user_id;
	
	private String order_time;

	private String pick_time;

	private String order_status;
	
	private String total_price;
	
	private String description;

	public OrderToJson(){
		
	}
	public OrderToJson(String order_id, String user_id, String order_time, String pick_time, String order_status,
			String total_price, String description) {
		super();
		this.order_id = order_id;
		this.user_id = user_id;
		this.order_time = order_time;
		this.pick_time = pick_time;
		this.order_status = order_status;
		this.total_price = total_price;
		this.description = description;
	}

	public String getOrder_id() {
		return order_id;
	}

	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getOrder_time() {
		return order_time;
	}

	public void setOrder_time(String order_time) {
		this.order_time = order_time;
	}

	public String getPick_time() {
		return pick_time;
	}

	public void setPick_time(String pick_time) {
		this.pick_time = pick_time;
	}

	public String getOrder_status() {
		return order_status;
	}

	public void setOrder_status(String order_status) {
		this.order_status = order_status;
	}

	public String getTotal_price() {
		return total_price;
	}

	public void setTotal_price(String total_price) {
		this.total_price = total_price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	
}
