package au.edu.sydney.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Product2")
public class Product2 {
	@Id
    @Column(name = "pnum")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int pnum;
	
	@Column(name = "pname")
    private String pname;
	
	@Column(name = "p_cnum")
    private int p_cnum;
	
	@Column(name = "quantity")
    private int quantity;
	
	@Column(name = "price")
    private Double price;
	
	@Column(name = "description")
    private String description;
	
	

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPnum() {
		return pnum;
	}

	public void setPnum(int pnum) {
		this.pnum = pnum;
	}

	public String getPname() {
		return pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}

	public int getP_cnum() {
		return p_cnum;
	}

	public void setP_cnum(int p_cnum) {
		this.p_cnum = p_cnum;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Product2 [pnum=" + pnum + ", pname=" + pname + ", p_cnum=" + p_cnum + ", quantity=" + quantity
				+ ", price=" + price + ", description=" + description + "]";
	}
	
	
}
