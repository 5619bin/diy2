package au.edu.sydney.domain;



public class PackageChioce {

	private int id;
	private String name;
	private Double price;
	
	private String description;

	
	public PackageChioce(){
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "PackageChioce [id=" + id + ", name=" + name + ", price=" + price + ", description=" + description + "]";
	}




	
}
