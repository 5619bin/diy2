package au.edu.sydney.web;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.Order;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Expression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import au.edu.sydney.dao.OrderDao;
import au.edu.sydney.dao.UserDao;
import au.edu.sydney.domain.Orders;
import au.edu.sydney.domain.User;

/**
 * Handles requests for the application home page.
 */
@Controller

//make the operations are transactional
@Transactional

@RequestMapping(value = "/user", method = RequestMethod.GET)
public class UserController extends HttpServlet{
	
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	 private User currentUser;

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signup(Locale locale, Model theModel) {
		
		//create a user object
		User theUser = new User();
		
		//add student object to the model
		theModel.addAttribute("user", theUser);
		
		
		return "sign_up";
	}
	
	@Autowired
	UserDao userDao;
	
	@RequestMapping(value = "/processSubmit")
	public String processSubmit(Locale locale, Model theModel,HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		//get data from front side
		String username = req.getParameter("userName");
		String useremail = req.getParameter("userEmail");
		String userphone = req.getParameter("UserPhone");
		String userpassword = req.getParameter("userPassword");
		
		//get all user list from database
		List<User> users = userDao.getUsers();
		
		//check if the user name is unique
		boolean b = false;
		for(int i=0; i<users.size(); i++){
			if(users.get(i).getUsername().equals(username)){
				b = true;
			}
		}
		if(b){
			theModel.addAttribute("error_infor", "This user name aready be used");
    		return "sign_up";
		}
		else{
			//create a user object
			User u = new User();
	        //set data 
			u.setUsername(username);
	        u.setUserphone(userphone);
	        u.setPassword(userpassword);
	        u.setUseremail(useremail);
	        //save data to database
	        userDao.saveUser(u);
	        //send data to front side
	        theModel.addAttribute("user", u);
			return "user-confirmation";
		}
		
//		//create a user object
//		User u = new User();
//        //set data 
//		u.setUsername(username);
//        u.setUserphone(userphone);
//        u.setPassword(userpassword);
//        u.setUseremail(useremail);
//        
//        //business logic
//        if(username.equals("aaa")){
//        	theModel.addAttribute("error_infor", "this user is prevented");
//    		return "sign_up";
//        }
//        
//        
//        userDao.saveUser(u);//save data to database
//        theModel.addAttribute("user", u);//send data to front side
//		return "user-confirmation";
	}
	
	@RequestMapping(value = "/signin", method = RequestMethod.GET)
	public String signin(Locale locale, Model theModel) {
		return "sign_in";
	}
	
	@RequestMapping(value = "/signinSubmit")
	public String signinSubmit(Locale locale, Model theModel,HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		//get data from front side
		String username = req.getParameter("username");
		String userpassword = req.getParameter("psw");
		System.out.println(username+"  :  "+userpassword);
		//create a user object
		System.out.println("going to get all users!!");
		List<User> users = userDao.getUsers();//invoking userDao function get the user list
		System.out.println("get all user successful!!");
		
		//select current user by user name
		User u;
		boolean b = false;
		for(int i=0; i<users.size(); i++){
			if(users.get(i).getUsername().equals(username)){
				u=users.get(i);
				if(users.get(i).getPassword().equals(userpassword)){
					b = true;
					theModel.addAttribute("user", u);
					currentUser=users.get(i);
				}
			}
		}
		if(b){
				return "welcome";
		}
		else{
			theModel.addAttribute("error_infor", "User name or password error!");
			return "sign_in";
		}
       
	}
	
	@RequestMapping(value = "/changeinfo", method = RequestMethod.GET)
	public String userinfo(Model theModel) {
		theModel.addAttribute("user", currentUser);
		System.out.println("before change" + currentUser.toString());
		return "change_info";
	}
	
	@RequestMapping(value = "/changepsw", method = RequestMethod.GET)
	public String userpsw(Locale locale, Model theModel) {
		theModel.addAttribute("user", currentUser);
		return "change_psw";//not created yet
	}
	
	@RequestMapping(value = "/userinfosave", method = RequestMethod.POST)
	public String userinfosave(@ModelAttribute("user") User theUser, Model theModel) {
		System.out.println("after change" + currentUser.toString());
		System.out.println("after change" + theUser.toString());
		if(theUser.getUseremail().isEmpty()||theUser.getUserphone().isEmpty()){
			theModel.addAttribute("error_infor", "Email and Phone can not be empaty!");
			theModel.addAttribute("user", currentUser);//send data of currentUser to front side
			return "change_info";
		}
		else{
			userDao.updateEmail(currentUser.getUserid(), theUser.getUseremail());
			userDao.updatePhone(currentUser.getUserid(), theUser.getUserphone());
			theModel.addAttribute("user", currentUser);
			return "welcome";
		}
	}
	
	@RequestMapping(value = "/userpswsave", method = RequestMethod.POST)
	public String userpswsave(@ModelAttribute("user") User theUser, Model theModel) {
		System.out.println("after change psw" + currentUser.toString());
		System.out.println("after change psw" + theUser.toString());
		if(theUser.getPassword().isEmpty()){
			theModel.addAttribute("error_infor", "Password can not be empaty!");
			theModel.addAttribute("user", currentUser);
			return "change_psw";
		}
		else if(theUser.getPassword().equals(currentUser.getPassword())){
			System.out.println("same password" + theUser.getPassword());
			theModel.addAttribute("error_infor", "Same password not allowed!");
			theModel.addAttribute("user", currentUser);
			return "change_psw";
		}
		else{
			userDao.updatePsw(currentUser.getUserid(),theUser.getPassword());
			theModel.addAttribute("user", currentUser);
			return "welcome";
		}
	}
	
	@Autowired
	OrderDao orderDao;
	
	
	
	@RequestMapping(value = "/orderhistoryuser", method = RequestMethod.GET)
	public String orderhistoryuser(Locale locale, Model theModel) {
		logger.info("Welcome list all order record: {}.", locale);
		
		ArrayList<Orders> currentOrders=new ArrayList<Orders>();
		
		List<Orders> orders=orderDao.getOrder();
		
		theModel.addAttribute("user", currentUser);
		
		for(int i=0; i<orders.size(); i++){
			if(orders.get(i).getUser_id()==currentUser.getUserid()&&orders.get(i).getUser_visiable()==1){
				currentOrders.add(orders.get(i));
			}
		}
		
		if(currentOrders.isEmpty()){
			theModel.addAttribute("error_infor", "No order");
			return "orderHistory_user";
		}
		else{
			theModel.addAttribute("order", currentOrders);
			return "orderHistory_user";
		}
	}
	
	
	
	@RequestMapping(value = "/userOrderDelete", method = RequestMethod.GET)
	public String userOrderDelet(Locale locale, Model theModel, @RequestParam("orderID") int theId) {
		logger.info("Welcome list all order record: {}.", locale);
		
		ArrayList<Orders> currentOrders=new ArrayList<Orders>();
		
		orderDao.updateVisiable(theId);
		
		List<Orders> orders=orderDao.getOrder();
		
		theModel.addAttribute("user", currentUser);
		
		for(int i=0; i<orders.size(); i++){
			if(orders.get(i).getUser_id()==currentUser.getUserid()&&orders.get(i).getUser_visiable()==1){
				currentOrders.add(orders.get(i));
			}
		}
		
		
		if(currentOrders.isEmpty()){
			theModel.addAttribute("error_infor", "No order");
			return "orderHistory_user";
		}
		else{
			theModel.addAttribute("order", currentOrders);
			return "orderHistory_user";
		}
	}
	
	@RequestMapping(value = "/orderIdSubmit", method = RequestMethod.GET)
	public String orderIdSubmit(Locale locale, Model theModel,HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		logger.info("Welcome list order record by order id: {}.", locale);
		theModel.addAttribute("user", currentUser);
		//get data from front side
		String orderid = req.getParameter("orderId");
		int orderId = Integer.parseInt(orderid);
		
		//get the order list form database
		
		List<Orders> orders=orderDao.getOrder();
		
		//select order by order id
		ArrayList<Orders> currentOrders=new ArrayList<Orders>();
		for(int i=0; i<orders.size(); i++){
			if(orders.get(i).getOrder_id()==orderId && orders.get(i).getUser_visiable()==1){
				currentOrders.add(orders.get(i));
			}
		}
		
		//return order list model & return page
		if(currentOrders.isEmpty()){
			theModel.addAttribute("error_infor", "No this order");
			return "orderHistory_user";
		}
		else{
			theModel.addAttribute("order", currentOrders);
			return "orderHistory_user";
		}

	}
	
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	public String welcome(Model theModel) {
		theModel.addAttribute("user", currentUser);
		return "welcome";
	}

}