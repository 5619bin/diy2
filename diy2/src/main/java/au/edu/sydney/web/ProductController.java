package au.edu.sydney.web;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import au.edu.sydney.dao.ProductDao;
import au.edu.sydney.domain.Product;

@Controller
@Transactional
@RequestMapping(value = "/product", method = RequestMethod.GET)
public class ProductController {
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	private Product currentProduct;
@Autowired
SessionFactory sessionFactory;
	 
@Autowired
ProductDao productDao;

@RequestMapping(value = "/addProduct", method = RequestMethod.GET)
public String product(Model model) {
    
			Product product = new Product();
			
			
			return "addProduct";
}

@RequestMapping(value = "/productSubmit")
public String productSubmit(Locale locale, Model theModel,HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
	//get data from front side
	String productname = req.getParameter("ProductName");
	String productcategory = req.getParameter("ProductCategory");
	String productquantity = req.getParameter("ProductQuantity");
	String productprice = req.getParameter("ProductPrice");
	String productdescription = req.getParameter("ProductDescription");
	List<Product> products= productDao.listproduct();

	Product p = new Product();
	int b = 0;
		for(int i=0; i<products.size(); i++){
			if(!products.get(i).getPname().equals(productname)){
				continue;
			}else{
				b = 1;
			}
		}
		
		if (b == 0){
			//set data 	
			p.setPname(productname);
			p.setP_cnum(Integer.parseInt(productcategory));
			p.setQuantity(Integer.parseInt(productquantity));
			p.setPrice(Double.parseDouble(productprice));
			p.setDescription(productdescription);

		    productDao.saveProduct(p);//save data to database
		    theModel.addAttribute("product", p);
		    //send data to front side
		    return "Company_home";
		}else{
			theModel.addAttribute("error_infor", "product error!");
			return "addProduct";
		}
		}

@RequestMapping(value = "/listProduct", method = RequestMethod.GET)
public String listProduct(Locale locale,Model model) {
    
	logger.info("Welcome list all order record: {}.", locale);
	
	List<Product> product=productDao.listproduct();
	
	
	model.addAttribute("product", product);
	
	return "list_product";
	
}
@RequestMapping(value = "/productDelete", method = RequestMethod.GET)
public String productDelete(Locale locale, Model theModel, @RequestParam("productID") int theId) {
	logger.info("List all the products: {}.", locale);
	
	//ArrayList<Product> currentProducts=new ArrayList<Product>();
	
	productDao.deleteProduct(theId);
	
	List<Product> product=productDao.listproduct();
	
	theModel.addAttribute("product", product);
	
	return "list_product";
}

Product currentP;

@RequestMapping(value = "/productUpdate", method = RequestMethod.GET)
public String productUpdate(Locale locale, Model theModel, @RequestParam("productID") int theId) {
	logger.info("List all the products: {}.", locale);
	
	//ArrayList<Product> currentProducts=new ArrayList<Product>();
	
	
	currentP=productDao.loadProduct(theId);
	
	
	theModel.addAttribute("product", currentP);
	
	return "updateProduct";
}


@RequestMapping(value = "/processUpdate", method = RequestMethod.POST)
public String processUpdate(Model theModel, @ModelAttribute("product") Product theProduct) {
	if(theProduct.getDescription().isEmpty()||theProduct.getPname().isEmpty()||String.valueOf(theProduct.getP_cnum())==null||
			String.valueOf(theProduct.getPrice())==null||String.valueOf(theProduct.getQuantity())==null){
		theModel.addAttribute("error_infor", "product attributes can not be empaty!");
		return "updateProduct";
	}else{
	productDao.updateName(currentP.getPnum(), theProduct.getPname());
	productDao.updateCategory(currentP.getPnum(), theProduct.getP_cnum());
	productDao.updatePrice(currentP.getPnum(), theProduct.getPrice());
	productDao.updateDescriptiony(currentP.getPnum(), theProduct.getDescription());
	productDao.updateQuantity(currentP.getPnum(), theProduct.getQuantity());

	List<Product> product=productDao.listproduct();
	
	theModel.addAttribute("product", product);
	
	return "list_product";}
}

@RequestMapping(value = "/homeCompany", method = RequestMethod.GET)
public String homeCompany(Model model) {
			
			return "Company_home";
}

}
