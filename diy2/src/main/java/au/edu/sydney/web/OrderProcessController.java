package au.edu.sydney.web;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import au.edu.sydney.dao.OrderDao;
import au.edu.sydney.dao.ProductAllDao;
import au.edu.sydney.dao.ProductDao;
import au.edu.sydney.domain.Cart;
import au.edu.sydney.domain.Orders;
import au.edu.sydney.domain.PackageChioce;
import au.edu.sydney.domain.Product;
import au.edu.sydney.domain.Product;
import au.edu.sydney.domain.User;
import au.edu.sydney.service.OrderService;
import au.edu.sydney.service.OrderServiceImpl;
import net.sf.cglib.core.Local;

@Controller
@Transactional
@RequestMapping(value = "/order", method = RequestMethod.GET)
public class OrderProcessController {
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	//define all needed Dao
	@Autowired
	OrderDao orderDao;
	
	@Autowired
	ProductDao productAllDao;
	
	Orders tempOrder;
	
	//define a cart used during ordering
	ArrayList<Cart> carts;
	
	@Autowired
	private OrderService orderService;
	
	//add order record by using service
	@RequestMapping(value="/addOrderReocrd", method = RequestMethod.GET)
	public String addOrder(Locale locale){
		logger.info("Welcome to add order : {}.", locale);
		
		orderService.addOrder();
		
		return "home";
	}
	
	//to list orders for check 
	@RequestMapping(value="/listOrders")
	public String listOrder(Locale locale, Model model){
		logger.info("Welcome list all order record: {}.", locale);
		
	//	List<Orders> orders=orderService.listOrder();
		List<Orders> orders=orderDao.getOrder();
		model.addAttribute("order", orders);
		
		return "list_order";
	}
	
//start a new order and need to get user id
	Integer currentUser;
	@RequestMapping("/newOrder/{id}")
	public String newOrder(Model theModel, @PathVariable("id") Integer userId) {
		//create a order record and construct new cart and 
		//store user ID to set it into order record
		currentUser=userId;
		tempOrder =new Orders();
		carts=new ArrayList<Cart>();

		// add order object to the model
		theModel.addAttribute("time", formTimeToShow(tempOrder.getOrder_time()));
		theModel.addAttribute("pickTime", formTimeToPick(tempOrder.getPick_time()));
		return "addOrderStepOne";
	}
	

		//when user choose choose more then he will invoke this controller
	
	@RequestMapping("/continueOrder")
	public String continueOrder(Model theModel) {
		
		
		//add pick up time to model to show your picked time in first choose page
		theModel.addAttribute("time", formTimeToShow(tempOrder.getOrder_time()));
		theModel.addAttribute("pickTime", formTimeToPick(tempOrder.getPick_time()));
		
		
		return "addOrderStepOne";
	}
	
	//after choose time and food type(package or DIY) 
	@RequestMapping(value="/processOne",method = RequestMethod.POST)
	public String processOne(Locale locale, HttpServletRequest req, Model model){
		String chioce = req.getParameter("chioce");
		String theTime = req.getParameter("theTime").replace('T', ' ')+":00";
		//create a user object
		System.out.println("theTime from bootstrap is"+ theTime);
		Timestamp time=Timestamp.valueOf(theTime);
		System.out.println("Time from bootstrap after to timestamp is"+ time);
		
		// valid time 
		if(time.before(tempOrder.getOrder_time())){
			model.addAttribute("time", tempOrder.getOrder_time().toString().substring(0, tempOrder.getOrder_time().toString().length()-4));
			model.addAttribute("message", "Please Choose a valid pick up time");			
			return "addOrderStepOne";
		}
			//valid working time
		if(time.getHours()<=9||time.getHours()>=21){
			model.addAttribute("time", tempOrder.getOrder_time().toString().substring(0, tempOrder.getOrder_time().toString().length()-4));
			model.addAttribute("message", "Please Choose a pick up time from 9a.m. to 9p.m.");			
			return "addOrderStepOne";
		}
		
		//go to different page judged by choice 
		if(chioce.equals("package")){       //go to package choose page
			logger.info("Welcome list all products: {}.", locale);
			//set pick up time to order
		tempOrder.setPick_time(time);
			System.out.println("the order time is :"+tempOrder.getOrder_time().toString().trim().substring(0, tempOrder.getOrder_time().toString().trim().length()-3));
			//get all packages from product
			
			ArrayList<PackageChioce> meal=getFood(1,1);

			//add data to model
			model.addAttribute("product", meal);
		
			return "choosePackage";
		}
		//else  go to diy page
		logger.info("Welcome list all DIYS: {}.", locale);
		//go to DIY choose page
		//get all diy components and add to model
		getDIY(model);
		
		System.out.println("the Order record time : " + tempOrder.getPick_time());
		return "chooseDIY";	
			
   
	}
	
	//after choose DIY 
	@RequestMapping(value = "/processBeforeDrink",method = RequestMethod.POST)
	public String processBeforeDrink(Model theModel,HttpServletRequest req, 
			@RequestParam(value = "bread", required = false) String bread,
			@RequestParam(value = "meat[]", required = false) String[] meat,
			@RequestParam(value = "cheese[]", required = false) String[] cheese,
			@RequestParam(value = "vegetable[]", required = false) String[] vegetable,
			@RequestParam(value = "source[]", required = false) String[] source,
			HttpServletResponse resp) throws ServletException, IOException{
		//get value from jsp and print it out
		//judge if user choose as last one bread 
		if(bread==null){
			String temp="please choose a bread";
			theModel.addAttribute("breadMessage", temp);
				getDIY(theModel);
			return "chooseDIY";
		}
			//set description and price to build 
			StringBuilder sb=new StringBuilder();
			Double price=0.0;

			Product theBread=productAllDao.loadProduct(Integer.parseInt(bread));
			price+=theBread.getPrice();
			sb.append(theBread.getPname()+"; ");
			
			ArrayList<String[]> diy=new ArrayList<String[]>();
			if(meat!=null)
				diy.add(meat);
			if(cheese!=null)
				diy.add(cheese);
			if(vegetable!=null)
				diy.add(vegetable);
			if(source!=null)
				diy.add(source);
			for(String[] st:diy){
				for(String str:st){
					Product pro=productAllDao.loadProduct(Integer.parseInt(str));
					price+=pro.getPrice();
					sb.append(pro.getPname()+", ");
				}	
			}
			
			//set items No as the No of carts +1
				Cart cart=new Cart("DIY",sb.delete(sb.length()-2, sb.length()).toString().trim(),1,price);
				cart.setNo(carts.size()+1);
				//put items into cart
				carts.add(cart);
			
			for(Cart c : carts)
				System.out.println(c.toString());
		
		//get drinks and snacks from all product
		ArrayList<PackageChioce> drinks=getFood(7,8);
		
		theModel.addAttribute("product", drinks);
		
		return "chooseDrinks";
	}
	
	
	//after choose DIY or package then choose a drink or snack
	@RequestMapping(value = "/processToDrink",method = RequestMethod.POST)
	public String processToDrink(Model theModel, @RequestParam(value = "position[]", required = false) String[] positions){
		//get value from jsp and print it out
		if(positions!=null){
			//for(String st:positions)
			//System.out.println(st);
			//List<Product> products=productAllDao.getAllProduct();
			
			for(String str:positions){
				//create a new cart	 		
				Product pro=productAllDao.loadProduct(Integer.parseInt(str));
				int temp=-1;
				for(Cart ca:carts){
					if(ca.getName().equals(pro.getPname()))
						temp=ca.getNo()-1;
				}
				if(temp==-1){
					Cart cart=new Cart(pro.getPname(),pro.getDescription(),1,pro.getPrice());
					cart.setNo(carts.size()+1);
					//put items into cart
					carts.add(cart);
				}else{
					carts.get(temp).setQuantity(carts.get(temp).getQuantity()+1);
				}
			}
			for(Cart c : carts)
				System.out.println(c.toString());
		}
		
		ArrayList<PackageChioce> product=getFood(7,8);
		theModel.addAttribute("product", product);
		
		return "chooseDrinks";
	}
	
	//after choose user has to confirm hie order of order more
	@RequestMapping(value="/processLast",method = RequestMethod.POST)
	public String processLast(Model theModel,@RequestParam(value = "position[]", required = false) String[] positions) {
		//create a user object
		if(positions!=null){
			for(String str:positions){
				//create a new cart
				Product pro=productAllDao.loadProduct(Integer.parseInt(str));
				int temp=-1;
				for(Cart ca:carts){
					if(ca.getName().equals(pro.getPname()))
						temp=ca.getNo()-1;
				}
				if(temp==-1){
					Cart cart=new Cart(pro.getPname(),pro.getDescription(),1,pro.getPrice());
					cart.setNo(carts.size()+1);
					//put items into cart
					carts.add(cart);
				}else{
					carts.get(temp).setQuantity(carts.get(temp).getQuantity()+1);
				}
			}
		}
	//	System.out.println(carts.size());
		tempOrder.setUser_id(1);
		//caculate total price
		Double total=0.00;
		for(Cart ca: carts){
			total+= (ca.getQuantity()*ca.getPrice());
		}

		
		tempOrder.setTotal_price(total);
		//add cart to model to show cart in page and user can view and delete 
		theModel.addAttribute("order",tempOrder);
		theModel.addAttribute("cart", carts);
		return "comfirmOrder";
		
	}
	
	//when user delete any thing in cart invoke this controller 
	@RequestMapping(value="/cartDelete",method = RequestMethod.POST)
	public String cartDelete(Model theModel,@RequestParam("cartNo") int cartNo) {
		
		if(cartNo<=carts.size())
			carts.remove(cartNo-1);
		//caculate total price
		Double total=0.00;
		for(int i=0;i<carts.size();i++){
			carts.get(i).setNo(i+1);
			total+=carts.get(i).getQuantity()*carts.get(i).getPrice();
		}
		tempOrder.setTotal_price(total);

		theModel.addAttribute("order",tempOrder);
		theModel.addAttribute("cart", carts);
		return "comfirmOrder";
		
	}
	
	
	//get paid
	@RequestMapping(value="/pay", method = RequestMethod.GET)
	public String pay(Locale locale, Model theModel) {
		//create a order record
		logger.info("Welcome home! The client locale is {}.", locale);
		System.out.println("current order id is:" +tempOrder.getOrder_id());
		
		theModel.addAttribute("order",tempOrder);
		
		return "pay";
	}
	
	//at last comfirm this order and wait company to comfrim
	@RequestMapping(value="/done", method = RequestMethod.GET)
	public String done(Locale locale, Model theModel) {
		
		logger.info("Welcome home! The client locale is {}.", locale);
		//convert carts information into String 
		StringBuilder sb=new StringBuilder();
		for(Cart ca: carts){
			sb.append(ca.getNo()+". "+ca.getName()+": "+ ca.getDescription()+"; "+"price: $"+ca.getPrice()+"\n");
		}
		//set order description with cart 
		tempOrder.setDescription(sb.toString());
		tempOrder.setUser_id(currentUser);
		System.out.println("in done process:\n"+tempOrder.toString());
		orderDao.saveOrder(tempOrder);
		if(orderDao.updateStatus(tempOrder.getOrder_id(), "paid"))
			System.out.println("commit successful");
		
		theModel.addAttribute("order",tempOrder);
		theModel.addAttribute("cart", carts);
		return "done";
	}
	
	
	@RequestMapping("/processForm")
	public String processSubmit(@ModelAttribute("order") Orders theOrder){
		
		// log the input data
		System.out.println("the description: " + theOrder.getDescription());
		System.out.println("theOrder: " + theOrder.getPick_time());
		
		return "comfirmOrder";
	}
	
	//get wanted product by using cateagory id 
	public ArrayList<PackageChioce> getFood(int start,int end){		
		List<Product> products=productAllDao.listproduct();
		
		ArrayList<PackageChioce> product=new ArrayList<PackageChioce>();
		for(int i=0;i<products.size();i++){
			if(products.get(i).getP_cnum()>=start&&products.get(i).getP_cnum()<=end){				
				PackageChioce p=new PackageChioce();
				p.setDescription(products.get(i).getDescription());
				p.setId(products.get(i).getPnum());
				p.setName(products.get(i).getPname());
				p.setPrice(products.get(i).getPrice());
				product.add(p);
			}
		}
		return product;
	}
	
	
	//get wanted product by using category id 
	public ArrayList<PackageChioce> getFood(int num){		
		List<Product> products=productAllDao.listproduct();
		
		ArrayList<PackageChioce> product=new ArrayList<PackageChioce>();
		for(int i=0;i<products.size();i++){
			if(products.get(i).getP_cnum()==num){				
				PackageChioce p=new PackageChioce();
				p.setDescription(products.get(i).getDescription());
				p.setId(products.get(i).getPnum());
				p.setName(products.get(i).getPname());
				p.setPrice(products.get(i).getPrice());
				product.add(p);
			}
		}
		return product;
	}
	
	//get all product needed in DIY proesss and add it into model
	 public void getDIY(Model model){
		 	
		 	//get bread
			ArrayList<PackageChioce> bread=getFood(2);
			
			//get meat
			
			ArrayList<PackageChioce> meat=getFood(3);
			
			//get cheese
			ArrayList<PackageChioce> cheese=getFood(4);
			
			//get vegetable
			ArrayList<PackageChioce> vegetable=getFood(5);

			//get source
			ArrayList<PackageChioce> source=getFood(6);
			
			for(PackageChioce p:source){
				System.out.println(p.toString());
			}
			model.addAttribute("bread", bread);
			model.addAttribute("meat", meat);
			model.addAttribute("cheese", cheese);
			model.addAttribute("vegetable", vegetable);
			model.addAttribute("source", source);
			
	 }
	
	 //form time to show in pick up choice
	 public String formTimeToPick(Timestamp time){
			SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd'T'HH:mm");
			
			System.out.println("Current Date: " + ft.format(time));
			
			String pickTime=ft.format(time);
			return pickTime;
	 }
	 
	 //form time to display
	 public String formTimeToShow(Timestamp time){
		 	System.out.println("timestamp :"+time.toString());
		 	
			SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd' 'HH:mm");
			
			System.out.println("Current Date: " + ft.format(time));
			
			String pickTime=ft.format(time);
			return pickTime;
	 }
	 
}
