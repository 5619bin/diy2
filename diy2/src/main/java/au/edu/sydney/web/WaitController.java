package au.edu.sydney.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import au.edu.sydney.dao.OrderDao;
import au.edu.sydney.domain.OrderToJson;
import au.edu.sydney.domain.Orders;
import au.edu.sydney.service.OrderServiceImpl;

@Controller
@RequestMapping(value = "/wait")
public class WaitController {
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	

	@Autowired
	private OrderDao orderDao; 

	@RequestMapping(value="/order")
	public String order(Locale locale, Model model){
		logger.info("Welcome to show new order: {}.", locale);
		
		Orders	theOrder=orderDao.loadOrder(68);
		List<Orders> a=orderDao.getNotComfirmedOrder();
		//a.add(theOrder);
		model.addAttribute("order", a);
		
		return "wait";
	}
	
	@RequestMapping(value="/getOrder",method = RequestMethod.GET)
	@ResponseBody
	public OrderToJson getOrder(Locale locale, Model model){
		//logger.info("Welcome to show new order: {}.", locale);
		
		List<Orders> newOrder=orderDao.getNotComfirmedOrder();
		OrderToJson order;
		if(newOrder.isEmpty()){
			//System.out.println("there is no order");
			order=new OrderToJson();
		}else{
			
		Orders theOrder=newOrder.get(0);
		 order=new OrderToJson(String.valueOf(theOrder.getOrder_id()), 
										String.valueOf(theOrder.getUser_id()),
										theOrder.getOrder_time().toString(),
										theOrder.getPick_time().toString(),
										String.valueOf(theOrder.getOrder_status()),
										String.valueOf(theOrder.getTotal_price()),
										theOrder.getDescription());
		
		//System.out.println("wait order"+theOrder.toString());
		}
		return order;
	}
	
	
	@RequestMapping(value = "/comfirm/{id}")
	public String comfirm(Locale locale, Model theModel,  @PathVariable("id") String orderId) {
		logger.info("Welcome to comfirm order: {}.", locale);
		
		orderDao.updateStatus(Integer.parseInt(orderId), "comfirmed");
		
		return "wait";
	}
	
}
