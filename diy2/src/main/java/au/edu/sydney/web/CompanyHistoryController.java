package au.edu.sydney.web;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Expression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.edu.sydney.dao.OrderDao;
import au.edu.sydney.dao.UserDao;
import au.edu.sydney.domain.Orders;
import au.edu.sydney.domain.Product;
import au.edu.sydney.domain.User;

/**
 * Handles requests for the application home page.
 */
@Controller

//make the operations are transactional
@Transactional

@RequestMapping(value = "/company", method = RequestMethod.GET)
public class CompanyHistoryController {
	
	private static final Logger logger = LoggerFactory.getLogger(CompanyHistoryController.class);
	

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	
	
	@Autowired
	OrderDao orderDao;
	
	
	
	@RequestMapping(value = "/orderhistory", method = RequestMethod.GET)
	public String listProduct(Locale locale,Model model) {
	    
		logger.info("Welcome list all order record: {}.", locale);
		
		List<Orders> orders=orderDao.getOrder();
		//System.out.println(orders.toString());
		
		model.addAttribute("order", orders);
		
		return "orderHistory_Company";
		
	}
	
	@RequestMapping(value = "/userIdSubmit", method = RequestMethod.GET)
	public String userIdSubmit(Locale locale, Model theModel,HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		logger.info("Welcome list order record by user id: {}.", locale);
		
		//get data from front side
		String userid = req.getParameter("userId");
		int userId = Integer.parseInt(userid);
		
		//get the order list form database
		
		List<Orders> orders=orderDao.getOrder();
		
		//select order by user id
		ArrayList<Orders> currentOrders=new ArrayList<Orders>();
		for(int i=0; i<orders.size(); i++){
			if(orders.get(i).getUser_id()==userId){
				currentOrders.add(orders.get(i));
			}
		}
		
		//return order list model & return page
		if(currentOrders.isEmpty()){
			theModel.addAttribute("error_infor", "No this user");
			return "orderHistory_Company";
		}
		else{
			theModel.addAttribute("order", currentOrders);
			return "orderHistory_Company";
		}

	}
	
	@RequestMapping(value = "/orderIdSubmit", method = RequestMethod.GET)
	public String orderIdSubmit(Locale locale, Model theModel,HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		logger.info("Welcome list order record by order id: {}.", locale);
		
		//get data from front side
		String orderid = req.getParameter("orderId");
		int orderId = Integer.parseInt(orderid);
		
		//get the order list form database
		
		List<Orders> orders=orderDao.getOrder();
		
		//select order by order id
		ArrayList<Orders> currentOrders=new ArrayList<Orders>();
		for(int i=0; i<orders.size(); i++){
			if(orders.get(i).getOrder_id()==orderId){
				currentOrders.add(orders.get(i));
			}
		}
		
		//return order list model & return page
		if(currentOrders.isEmpty()){
			theModel.addAttribute("error_infor", "No this order");
			return "orderHistory_Company";
		}
		else{
			theModel.addAttribute("order", currentOrders);
			return "orderHistory_Company";
		}

	}
	
	@RequestMapping(value = "/signin", method = RequestMethod.GET)
	public String signin(Locale locale, Model theModel) {
		return "company_signIn";
	}
	
	@RequestMapping(value = "/signinSubmit")
	public String signinSubmit(Locale locale, Model theModel,HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		//get data from front side
		String name = req.getParameter("name");
		String password = req.getParameter("psw");
		System.out.println(name+"  :  "+password);
		//check name and password if not correct return error info
		boolean b = false;
		if(name.equals("admin")&&password.equals("admin")){
			return "Company_home";
		}
		else{
			theModel.addAttribute("error_infor", "User name or password error!");
			return "company_signIn";
		}  
	}
	
}