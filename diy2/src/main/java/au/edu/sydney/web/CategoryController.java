package au.edu.sydney.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import au.edu.sydney.dao.CategoryDao;
import au.edu.sydney.dao.OrderDao;
import au.edu.sydney.dao.ProductDao;
import au.edu.sydney.domain.Category;
import au.edu.sydney.domain.Orders;
import au.edu.sydney.domain.Product;

@Controller
@Transactional
@RequestMapping(value = "/category", method = RequestMethod.GET)
public class CategoryController {
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	@Autowired
	SessionFactory sessionFactory;
		 
	@Autowired
	CategoryDao categoryDao;
	
	@RequestMapping(value = "/addCategory", method = RequestMethod.GET)
	public String category(Locale locale, Model model) {
		
		Category category = new Category();
	    
	    return "addCategory";
	}
	
	@RequestMapping(value = "/categorySubmit")
	public String categorySubmit(Locale locale, Model theModel,HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		//get data from front side
		
		String categoryname = req.getParameter("CategoryName");
		List<Category> categories= categoryDao.listcategory();

		Category c = new Category();
		int b = 0;
			for(int i=0; i<categories.size(); i++){
				if(!categories.get(i).getCname().equals(categoryname)){
					continue;
				}else{
					b = 1;
				}
			}
			
			if (b == 0){
				//set data 	
				c.setCname(categoryname);
				
			    categoryDao.saveCategory(c);//save data to database

			    theModel.addAttribute("category", c);

			    //send data to front side
			    return "Company_home";
			}else{
				theModel.addAttribute("error_infor", "categoryname error!");
				return "addCategory";
			}
		}
	
	@RequestMapping(value = "/listCategory", method = RequestMethod.GET)
	public String listCategory(Locale locale,Model model) {
	    
		logger.info("Welcome list all category record: {}.", locale);
		
		List<Category> category= categoryDao.listcategory();

		
		model.addAttribute("category", category);
		
		return "list_category";
		
	}
	
	
	@Autowired
	ProductDao productDao;
	@RequestMapping(value = "/categoryDelete", method = RequestMethod.GET)
	public String categoryDelete(Locale locale, Model theModel, @RequestParam("categoryID") int theId) {
		logger.info("List all the categories: {}.", locale);
		//delete category
		categoryDao.deleteCategory(theId);
		
		List<Category> category=categoryDao.listcategory();
		
		theModel.addAttribute("category", category);

		
		//delete product
		int a;
		List<Product> product=productDao.listproduct();
		for(int i=0;i<product.size();i++){
			if(product.get(i).getP_cnum()==theId){
				a=product.get(i).getPnum();
				productDao.deleteProduct(a);
			}
		}
		product=productDao.listproduct();				
		
		return "list_category";
	}

	Category currentC;

	@RequestMapping(value = "/categoryUpdate", method = RequestMethod.GET)
	public String categoryUpdate(Locale locale, Model theModel, @RequestParam("categoryID") int theId) {
		logger.info("List all the products: {}.", locale);
		
		//ArrayList<Product> currentProducts=new ArrayList<Product>();
		
		
		currentC=categoryDao.loadCategory(theId);
		
		
		theModel.addAttribute("category", currentC);
		
		return "updateCategory";
	}


	@RequestMapping(value = "/processUpdateCategory", method = RequestMethod.POST)
	public String processUpdateCategory(Model theModel, @ModelAttribute("category") Category theCategory) {
		if(theCategory.getCname().isEmpty()){
			theModel.addAttribute("error_infor", "category attributes can not be empaty!");
			return "updateCategory";
		}else{
		categoryDao.updateNamec(currentC.getCnum(), theCategory.getCname());

		List<Category> category=categoryDao.listcategory();
		
		theModel.addAttribute("category", category);
		
		return "list_category";
		}
	}
}
