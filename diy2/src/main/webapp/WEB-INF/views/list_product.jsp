<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>

<html>

<head>
	<title>List Products</title>
	
	<!-- reference our style sheet -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	

	<link type="text/css"
		  rel="stylesheet"
		  href="${pageContext.request.contextPath}/resources/css/style.css" />

</head>

<body class="text-center">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">DIY Order</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent" >
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="\sydney\product\homeCompany">Home <span class="sr-only">(current)</span></a>
      </li>
      
      <div class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Category Management
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="\sydney\category\addCategory">Add Category</a>
          <a class="dropdown-item" href="\sydney\category\listCategory">Category List(Delete/Update)</a>    
        </div>
      </div>
      
            <div class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Product Management
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="\sydney\product\addProduct">Add Product</a>
          <a class="dropdown-item" href="\sydney\product\listProduct">Product List(Delete/Update)</a>    
        </div>
      </div>
      <li class="nav-item active">
        <a class="nav-link" href="\sydney\company\orderhistory">Order History <span class="sr-only">(current)</span></a>
      </li>
      
    </ul>
    	<div class="nav-item"> 
    	<a class="nav-link" href="\sydney\wait\order">Note</a> </div>
   
  </div>
</nav>


	<div id="wrapper">
		<div id="header">
			<h2>Product List</h2>
		</div>
	</div>
	
	<div id="container">
	
		<div id="content">
		
			<!--  add our html table here -->
		
			<table class="table table-bordered">
				<tr>
					<th class="text-center">Product ID</th>
					<th class="text-center">Product Name</th>
					<th class="text-center">Product Category</th>
					<th class="text-center">Product Quantity </th>
					<th class="text-center">Product Price</th>
					<th class="text-center">Product Description</th>
					<th class="text-center">Delete</th>
					<th class="text-center">Update</th>
				</tr>
				
				<!-- loop over and print our orders -->
				<c:forEach var="tempProduct" items="${product}">
				 <c:url var="productDeleteLink" value="/product/productDelete">
				 
					 	<c:param name="productID" value="${tempProduct.pnum}"/>
					 </c:url>
					 <c:url var="productUpdateLink" value="/product/productUpdate">
				 
					 	<c:param name="productID" value="${tempProduct.pnum}"/>
					 </c:url>
					<tr>
						<td> ${tempProduct.pnum} </td>
						<td> ${tempProduct.pname} </td>
						<td> ${tempProduct.p_cnum} </td>
						<td> ${tempProduct.quantity} </td>
						<td> ${tempProduct.price} </td>
						<td> ${tempProduct.description} </td>
						
						<td>
							<!-- display the update link -->
							<a href="${productDeleteLink}">Delete</a>
						</td>
						<td>
							<!-- display the update link -->
							<a href="${productUpdateLink}">Update</a>
						</td>
					</tr>
				
				</c:forEach>
						
			</table>
				
		</div>
	
	</div>
	
<!-- bootstrap css -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

</body>

</html>









