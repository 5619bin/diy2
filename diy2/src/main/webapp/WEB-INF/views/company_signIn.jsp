<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>

<head>
	<title>Company sign in</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
</head>

<style>
body {   width: 400px;
		 margin: 0 auto;}
</style>

<body>
<form action="/sydney/company/signinSubmit">
<table class="text-center">
	<tr>
		<td colspan="2">
			<h1>Welcome!</h1><br><br>
		</td>
	<tr>
		<td colspan="2">Log in and management your orders!</td>
	</tr>
	<tr>
		<td colspan="2"><br><br><i style="background-color : rgb(255,0,0)"> ${error_infor}</i></td>
	</tr>
	<tr>
		<td><br><br>Name:<br><br></td>
		<td><br><br><input type="text" name="name"><br><br></td>
	</tr>
	<tr>
		<td>Password:<br><br></td>
		<td><input type="password" name="psw"><br><br></td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" value="submit" class="btn btn-primary"><br><br></td>
	</tr>
</table>

</form>
 </P>
 
 	
	
</body>
</html>