<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>

<html>

<head>
	<title>List Products</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	
	<!-- reference our style sheet -->

<style>
body {   width: 900px;
		 margin: 0 auto;}
</style>

</head>

<body class="text-center">

	
	<div id="container">
	
		<div id="content">
		
			<!--  add our html table here -->
		
			<table class="table table-bordered">
				<tr>
   					 <td colspan="7"><h1>Oder</h1></td>
  				</tr>
  				<!-- need to change the URL -->
  				<tr class="text-right">
  					<td colspan="7"><a href="<c:url value="\sydney\product\homeCompany"/>" class="badge badge-primary">Home</a></td>
				</tr>
					<td colspan="7">
						<form action="/sydney/company/userIdSubmit">
							<input type="text" name="userId" placeholder="Search by user id">
							<input type="submit" value="Search" class="btn btn-primary">
						</form><br>
						<form action="/sydney/company/orderIdSubmit">
							<input type="text" name="orderId" placeholder="Search by order id">
							<input type="submit" value="Search" class="btn btn-primary">
						</form>
					</td>
				</tr>
				<tr>
   					 <td colspan="7">error information: <i style="background-color : rgb(255,0,0)"> ${error_infor}</i></td>
  				</tr>
				<tr class="text-white bg-primary">
					<th class="text-center">Order ID</th>
					<th class="text-center">User ID</th>
					<th class="text-center">Order Time</th>
					<th class="text-center">Pick Up Time </th>
					<th class="text-center">Order Status</th>
					<th class="text-center">Total Price</th>
					<th class="text-center">Description</th>
				</tr>
				
				<!-- loop over and print our orders -->
				<c:forEach var="tempOrder" items="${order}">
				
					<tr>
						<td> ${tempOrder.order_id} </td>
						<td> ${tempOrder.user_id} </td>
						<td> ${tempOrder.order_time} </td>
						<td> ${tempOrder.pick_time} </td>
						<td> ${tempOrder.order_status} </td>
						<td> ${tempOrder.total_price} </td>
						<td> ${tempOrder.description} </td>
						
					</tr>
				
				</c:forEach>
						
			</table>
				
		</div>
	
	</div>
	

</body>

</html>









