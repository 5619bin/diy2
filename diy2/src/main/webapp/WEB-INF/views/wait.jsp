<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>

<html>


<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	<title>Wait for Order </title>

<script type="text/javascript" src="http://ajax.microsoft.com/ajax/jquery/jquery-1.4.min.js"></script>
</head>
<style>
table {   width: 900px;
		 margin: 0 auto;}
</style>

<body class="text-center">
<!-- navbar part -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">DIY Order</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent" >
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="\sydney\user\welcome">Home <span class="sr-only">(current)</span></a>
      </li>
      
      <div class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Personal Informations
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="\sydney\user\changeinfo">Change Information</a>
          <a class="dropdown-item" href="\sydney\user\changepsw">Change Password</a>    
        </div>
      </div>
    </ul>
    	<div class="nav-item"> 
    	<a class="nav-link" href="\sydney\user\signin">Log out</a> </div>
   
  </div>
</nav>
	
	<div id="container">
	
		<div id="content">
		
			<!--  add our html table here -->
		
			<table class="table table-bordered">
				<tr>
					<td colspan="8"><br><h2>New Order displayer<br></h2></td>
				</tr>
				<tr class="text-right">
  					<td colspan="8"><a href="<c:url value="\sydney\product\homeCompany"/>" class="badge badge-primary">Home</a></td>
				</tr>
				
				<tr class="text-white bg-primary">
					<th class="text-center">order id</th>
					<th class="text-center">user id</th>
					<th class="text-center">order time</th>
					<th class="text-center">pick up time </th>
					<th class="text-center">order status</th>
					<th class="text-center">total price</th>
					<th class="text-center">description</th>
					<th class="text-center">Action</th>
				</tr>
			
					
				<tr >
					<th id="order_id" class="text-center"></th>
					<th id="user_id" class="text-center"></th>
					<th id="order_time" class="text-center"></th>
					<th id="pick_time" class="text-center"> </th>
					<th id="order_status" class="text-center"></th>
					<th id="total_price" class="text-center"></th>
					<th id="description"class="text-center"></th>
					<td><button onclick="comfirmOrder()">comfirm and print</button></td>
				</tr>
					
			</table>
				
		</div>
	
	</div>
	
	
<script>
var order_id;
function loaData(){
$.ajax({
    url: "http://localhost:8080/sydney/wait/getOrder",
    type: "GET",
    contentType: 'application/json',
    dataType: 'json',
    success: function (data) {
    /* 	$("#order_id").append(data.order_id);
    	$("#user_id").append(data.user_id);
    	$("#order_time").append(data.order_time);
    	$("#pick_time").append(data.pick_time);
    	$("#order_status").append(data.order_status);
    	$("#total_price").append(data.total_price); */
    	if(data==null)
    		order_id=0;
    	else{
	    	document.getElementById("order_id").innerHTML=data.order_id;
	    	document.getElementById("user_id").innerHTML=data.user_id;
	    	document.getElementById("order_time").innerHTML=data.order_time;
	    	document.getElementById("pick_time").innerHTML=data.pick_time;
	    	document.getElementById("order_status").innerHTML=data.order_status;
	    	document.getElementById("total_price").innerHTML=data.total_price;
	    	document.getElementById("description").innerHTML=data.description;
	    	order_id=data.order_id;
    	}
    	
    },
    error: function(data){
    	alert(data.responseText);
    }
})
}

function comfirmOrder(){
	$.ajax({
	    url: "http://localhost:8080/sydney/wait/comfirm/"+order_id,
	    type: "GET",
	    contentType: 'application/json',
	    dataType: 'json',
	    success: function (data) {
	   
	    	document.getElementById("comfirm").innerHTML=data.responseText;
	    	
	    },
	    error: function(data){
	    	alert(data.responseText);
	    }
	})
	}
	

$(function(){
    setInterval('loaData()', 2000);  
});
 
function loadData()
{
  var xmlhttp;
  if (window.XMLHttpRequest)
  {   
    xmlhttp=new XMLHttpRequest();
  }
  else
  {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.open("GET","http://localhost:8080/sydney/wait/getOrder",true);
  xmlhttp.send();
  
  xmlhttp.onreadystatechange=function()
  {
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
      document.getElementById("description").innerHTML=xmlhttp.reponseText;
    }
  }
}
</script>
	

</body>

</html>









