<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>

<!DOCTYPE html>

<html>

<head>
	<title>sign up</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
</head>
<style>
body {   width: 400px;
		 margin: 0 auto;}
</style>
<body>
<h1 class="text-center">
	Welcome to join us! 
</h1>

<P class="text-center">  
<b>Create a New Account!<br></b>

	<i style="background-color : rgb(255,0,0)"> ${error_infor}</i>
	<!-- creat the table for sign up -->
	<form action="/sydney/user/processSubmit">
		<br><br>
		<table>
			<tr>
				<td>User Name*:<br><br></td>
				<td><input type="text" name="userName" required><br><br></td>
			</tr>
			<tr>
				<td>Email*:<br><br></td>
				<td><input type="email" name="userEmail" required><br><br></td>
			</tr>
			<tr>
				<td>Mobile Phone*:<br><br></td>
				<td><input type="text" name="UserPhone" required><br><br></td>
			</tr>
			<tr>
				<td>Password*:<br><br></td>
				<td><input type="password" name="userPassword" required><br><br></td>
			</tr>
			<tr>
				<td><input type="submit" value="Submit" class="btn btn-primary"><br><br></td>
				<td><a href="<c:url value="\sydney\user\signin"/>" class="badge badge-light">Already Have</a>
				    <a href="<c:url value="\sydney"/>" class="badge badge-light">Home</a><br><br>
				</td>
			</tr>
		</table>
	</form>
		
		<br><br>

 </P>
</body>
</html>