<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>

<head>
	<title>Home</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
</head>



<body class="p-3 mb-2 bg-secondary text-white">
<h1 class="text-center">
	Welcome to Our DIY Order!  
</h1>

<div class="text-center">
	<br><br>
	<p>	No time? No kitchen? Cannot cook? </P>
	<p>  Don't worry, we can help you! </P>
	<p> Just need 5 minute, you can get your DIY meal. </P>
	<p> Let's do it ! </P>
	



</div>

<div class="text-center">
	<br><br>
 	   <a href="<c:url value="\sydney\user\signin"/>" class="badge badge-pill badge-primary">User Log In</a>
 	   
 	   <a href="<c:url value="\sydney\user\signup"/>" class="badge badge-pill badge-primary">User Sign Up</a>
 	   <br><br> <!-- need to change the path -->
 	   <a href="<c:url value="\sydney\company\signin"/>" class="badge badge-secondary">Company</a>  

</div>



</body>
</html>
