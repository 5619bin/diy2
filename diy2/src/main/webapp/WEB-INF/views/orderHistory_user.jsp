<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>

<html>


<head>
	<title>Order History</title>
	
	<!-- reference our style sheet -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
<style>
table {   width: 900px;
		 margin: 0 auto;}
</style>
	
</head>


<body class="text-center">
<!-- navbar part -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">DIY Order</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent" >
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="\sydney\user\welcome">Home <span class="sr-only">(current)</span></a>
      </li>
      
      <div class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Personal Informations
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="\sydney\user\changeinfo">Change Information</a>
          <a class="dropdown-item" href="\sydney\user\changepsw">Change Password</a>    
        </div>
      </div>
    </ul>
    	<div class="nav-item"> 
    	<a class="nav-link" href="\sydney\user\signin">Log out</a> </div>
   
  </div>
</nav>
	
	<div id="container">
	
		<div id="content">
		
			<!--  add our html table here -->
		
			<table class="table table-bordered">
				<tr>
					<td colspan="8"><br><h2>${user.username}'s Order History<br></h2></td>
				</tr>
				<tr class="text-right">
  					<td colspan="8"><a href="<c:url value="\sydney\user\welcome"/>" class="badge badge-primary">Home</a></td>
				</tr>
				<tr>
					<td colspan="8">
						<form action="/sydney/user/orderIdSubmit">
							<input type="text" name="orderId" placeholder="Search by order id">
							<input type="submit" value="Search" class="btn btn-primary">
						</form>
					</td>
				</tr>
				<tr>
					<td colspan="8"><i style="background-color : rgb(255,0,0)"> ${error_infor}</i></td>
				</tr>
				<tr class="text-white bg-primary">
					<th class="text-center">order id</th>
					<th class="text-center">user id</th>
					<th class="text-center">order time</th>
					<th class="text-center">pick up time </th>
					<th class="text-center">order status</th>
					<th class="text-center">total price</th>
					<th class="text-center">description</th>
					<th class="text-center">Action</th>
				</tr>
				
				<!-- loop over and print our orders -->
				<c:forEach var="tempOrder" items="${order}">
					 
					 <!-- construct an "update" link with order id -->
					 <c:url var="userDeletLink" value="/user/userOrderDelete">
					 	<c:param name="orderID" value="${tempOrder.order_id}"/>
					 </c:url>
					 
					<tr>
						<td> ${tempOrder.order_id} </td>
						<td> ${tempOrder.user_id} </td>
						<td> ${tempOrder.order_time} </td>
						<td> ${tempOrder.pick_time} </td>
						<td> ${tempOrder.order_status} </td>
						<td> ${tempOrder.total_price} </td>
						<td> ${tempOrder.description} </td>
						
						<td>
							<!-- display the update link -->
							<a href="${userDeletLink}">Delete</a>
						</td>
						
					</tr>
				
				</c:forEach>
						
			</table>
				
		</div>
	
	</div>
	
<!-- bootstrap css -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

</body>

</html>









