<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>

<head>
	<title>sign in</title>
	<!-- add the link for use bootstrap -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
</head>

<style>
body {   width: 500px;
		 margin: 0 auto;}
</style>

<body>
<form action="/sydney/user/signinSubmit">
<table class="text-center">
	<tr>
		<td colspan="2">
			<h1>Welcome to DIY order!</h1><br><br>
		</td>
	<tr>
		<td colspan="2">Let's do your own burger and have health life!</td>
	</tr>
	<tr>
		<td colspan="2"><br><br><i style="background-color : rgb(255,0,0)"> ${error_infor}</i></td>
	</tr>
	<tr>
		<td><br><br>User Name:<br><br></td>
		<td><br><br><input type="text" name="username"><br><br></td>
	</tr>
	<tr>
		<td>Password:<br><br></td>
		<td><input type="password" name="psw"><br><br></td>
	</tr>
	<tr>
		<td><input type="submit" value="submit" class="btn btn-primary"><br><br></td>
		<td>
			<a href="<c:url value="\sydney\user\signup"/>" class="badge badge-light">New Account</a>&nbsp;&nbsp;
			<a href="<c:url value="\sydney"/>" class="badge badge-light">Home</a><br><br>
		</td>
	</tr>
</table>

</form>
 </P>
 
 	
	
</body>
</html>