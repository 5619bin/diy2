<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page session="false" %>

<!DOCTYPE html>

<html>
<body style="text-align:center;">

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">DIY Order</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent" >
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="\sydney\product\homeCompany">Home <span class="sr-only">(current)</span></a>
      </li>
      
      <div class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Category Management
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="\sydney\category\addCategory">Add Category</a>
          <a class="dropdown-item" href="\sydney\category\listCategory">Category List(Delete/Update)</a>    
        </div>
      </div>
      
            <div class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Product Management
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="\sydney\product\addProduct">Add Product</a>
          <a class="dropdown-item" href="\sydney\product\listProduct">Product List(Delete/Update)</a>    
        </div>
      </div>
      <li class="nav-item active">
        <a class="nav-link" href="\sydney\company\orderhistory">Order History <span class="sr-only">(current)</span></a>
      </li>
      
    </ul>
    	<div class="nav-item"> 
    	<a class="nav-link" href="\sydney\wait\order">Note</a> </div>
   
  </div>
</nav>
<div style="margin:20px auto"></div>

<head>
	<title>update product</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
</head>
<body style="text-align:center;">
<div style="margin:20px auto"></div>

<h1>
	Product management 
</h1>

<P>  update a product!
 </P>
 
 <p>
 <form:form action="/sydney/product/processUpdate" modelAttribute="product">
	
		product id: ${product.pnum}
		
		<br><br>
		&emsp;&emsp;&nbsp;product name:<form:input path="pname" /><br><br>
	
		&nbsp;&nbsp;product category:<form:input path="p_cnum" /><br><br>
	
		&emsp;product quantity:<form:input path="quantity" /><br><br>
		
		&emsp;&emsp;&nbsp;&nbsp;product price:<form:input path="price" /><br><br>
		
		product description:<form:input path="description" /><br><br>
	
	
		<input type="submit" value="Save" class="btn btn-primary"/><br><br>
		
				product category:1	Package; 2	Bread; 3	Meat; 4	 Cheese; 5	Vegetable; 6	Source; 7	Drink; 8	Snack
		
	
	</form:form>
 </p>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
 
</body>
</html>

