<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>

<html>

<head>
	<title>List Orders</title>
	
	<!-- reference our style sheet -->

	<link type="text/css"
		  rel="stylesheet"
		  href="${pageContext.request.contextPath}/resources/css/style.css" />

</head>

<body>

	<div id="wrapper">
		<div id="header">
			<h2>list  persons</h2>
		</div>
	</div>
	
	<div id="container">
	
		<div id="content">
		
			<!--  add our html table here -->
		
			<table>
				<tr>
					<th>order id</th>
					<th>user id</th>
					<th>order time</th>
					<th>pick up time </th>
					<th>order status</th>
					<th>total price</th>
					<th>description</th>
					<th>user visiable</th>
				</tr>
				
				<!-- loop over and print our orders -->
				<c:forEach var="tempOrder" items="${order}">
				
					<tr>
						<td> ${tempOrder.order_id} </td>
						<td> ${tempOrder.user_id} </td>
						<td> ${tempOrder.order_time} </td>
						<td> ${tempOrder.pick_time} </td>
						<td> ${tempOrder.order_status} </td>
						<td> ${tempOrder.total_price} </td>
						<td> ${tempOrder.description} </td>
						<td> ${tempOrder.user_visiable} </td>
						
					</tr>
				
				</c:forEach>
						
			</table>
				
		</div>
	
	</div>
	

</body>

</html>









