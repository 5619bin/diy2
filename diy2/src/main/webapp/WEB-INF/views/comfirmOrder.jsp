<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>

<html>

<head>
	<title>Your Order has Confirmed</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" 
	integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	
</head>

<style>
div {   width: 480px;
		 margin: 0 auto;}
</style>


<body>
<!--  for  navbar -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">DIY Order</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="\sydney\user\welcome">Home <span class="sr-only">(current)</span></a>
      </li>
      
      <div class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Personal Informations
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="\sydney\user\changeinfo">Change Information</a>
          <a class="dropdown-item" href="\sydney\user\changepsw">Change Password</a>    
        </div>
      </div>
    </ul>
    	<div class="nav-item"> 
    	<a class="nav-link" href="\sydney\user\signin">Log out</a> </div>
   
  </div>
</nav>

<h1 class="text-center">Your Order has Confirmed</h1>
<br>
<p class="text-center">The pick time is confirmed: ${order.pick_time}</p>
<br> 
<p class="text-center">your total price is: $ ${order.total_price}</p>
<br>
<div >
your content is: <br>
<div>
						
				<c:forEach var="carts" items="${cart}">
					 <c:url var="cartDeletLink" value="/order/cartDelete">
					 	<c:param name="cartNo" value="${carts.no}"/>
					 </c:url>
					<tr>
						<td> ${carts.no} :</td> 
						<td> ${carts.name} :</td> 
						<td> ${carts.description} ; price: $</td>
						<td> ${carts.price} Quantity:</td>
						<td> ${carts.quantity}</td>
						<td>
							<!-- display the update link -->
							<a href="${cartDeletLink}">Delete</a>
						</td>
						<br>
					</tr>
				
				</c:forEach>
</div>

<br><br>

<a href="<c:url value="\sydney\order\continueOrder"/>" class="btn btn-primary">order more</a>

<a href="<c:url value="\sydney\order\pay"/>" class="btn btn-primary">comfirm and pay</a>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

</body>

</html>







