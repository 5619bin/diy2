<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<!DOCTYPE html>

<html>

<head>
	<title>home</title>
	<title>choose package</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" 
	integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	
</head>

<style>
form {   width: 480px;
		 margin: 0 auto;}
</style>

<body>
<!--  for  navbar -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">DIY Order</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="\sydney\user\welcome">Home <span class="sr-only">(current)</span></a>
      </li>
      
      <div class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Personal Informations
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="\sydney\user\changeinfo">Change Information</a>
          <a class="dropdown-item" href="\sydney\user\changepsw">Change Password</a>    
        </div>
      </div>
    </ul>
    	<div class="nav-item"> 
    	<a class="nav-link" href="\sydney\user\signin">Log out</a> </div>
   
  </div>
</nav>

<h1 class="text-center"> Welcome !</h1>
<br>
<h3 class="text-center"> Please Choose Your DIY Sandwich!</h3>

<form:form action="processBeforeDrink">
Make Your Choice

		<br><br>
		<ul>Bread:
			<c:forEach var="temp" items="${bread}">
			 <p>   <input type="radio" name="bread" value="${temp.id}" checked> ${temp.name} 
			     price: ${temp.price} description: ${temp.description}  
			     </p>
			</c:forEach>
			<br>			Meat:
			<c:forEach var="temp" items="${meat}">
			 <p>   <input type="checkbox" name="meat[]" value="${temp.id}"> ${temp.name} 
			     price: ${temp.price} description: ${temp.description}  
			     </p>
			</c:forEach>
			<br>Cheese:
			<c:forEach var="temp" items="${cheese}">
			 <p>   <input type="checkbox" name="cheese[]" value="${temp.id}"> ${temp.name} 
			     price: ${temp.price} description: ${temp.description}  
			     </p>
			</c:forEach>
			<br>Vegetable:
			<c:forEach var="temp" items="${vegetable}">
			 <p>   <input type="checkbox" name="vegetable[]" value="${temp.id}"> ${temp.name} 
			     price: ${temp.price} description: ${temp.description}  
			     </p>
			</c:forEach>
			<br>Source:
			<c:forEach var="temp" items="${source}">
			 <p>   <input type="checkbox" name="source[]" value="${temp.id}"> ${temp.name} 
			     price: ${temp.price} description: ${temp.description}  
			     </p>
			</c:forEach>
			<div style="background-color : rgb(255,0,0)"> ${breadMessage}</div>
		</ul>
	<br>
	<input type="submit" value="next"  class="btn btn-primary">
</form:form>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

</body>

</html>












