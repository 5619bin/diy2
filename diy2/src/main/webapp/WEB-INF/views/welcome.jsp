<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
<title>welcome</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" 
	integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">


</head>

<style>
table {   width: 300px;
		 margin: 0 auto;}
</style>


<body>
   
<!-- navbar part -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">DIY Order</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent" >
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="\sydney\user\welcome">Home <span class="sr-only">(current)</span></a>
      </li>
      
      <div class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Personal Informations
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="\sydney\user\changeinfo">Change Information</a>
          <a class="dropdown-item" href="\sydney\user\changepsw">Change Password</a>    
        </div>
      </div>
    </ul>
    	<div class="nav-item"> 
    	<a class="nav-link" href="\sydney\user\signin">Log out</a> </div>
   
  </div>
</nav>
  
       <table class="text-center">
       		<tr>
       			<td><br><br><h2 style="text-align:center;">Welcome ${user.username}<br><br></h2> </td>
       		</tr>
       		<tr>
       			<td>
       				<form action="/sydney/order/newOrder/${user.userid}">
						<input type="submit" value="Start Your Order" class="btn btn-outline-primary">
						<br><br><br>
					</form>
				</td>
			</tr>
			<tr>
				<td>
					<form action="/sydney/user/changeinfo">
						<input type="submit" value="Change Your Information" class="btn btn-outline-success">
						<br><br><br>
					</form>
				</td>
			</tr>
			<tr>
				<td>
					<form action="/sydney/user/changepsw">
						<input type="submit" value="Change Your Password" class="btn btn-outline-warning">
						<br><br><br>
					</form>
				</td>
			</tr>
			<tr>
				<td>
					 <form action="/sydney/user/orderhistoryuser">
						<input type="submit" value="Your Order History" class="btn btn-outline-danger">
						<br><br><br>
					 </form>
				</td>
			</tr>
		</table>
		
		<!-- bootstrap css -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
		
		
	</body>
</html>
			