package diy2;

import au.edu.sydney.domain.User;
import junit.framework.Test;
import junit.framework.TestResult;

import junit.framework.TestCase;

public class UserDomainTest extends TestCase {
	
	private User user;

	@Override
	protected void setUp() throws Exception {
		// TODO Auto-generated method stub
		user =new User();
	}
	
	public void testSetAndGetUseremail(){
		String testUseremail ="123@456.com";
		assertNull(user.getUseremail());
		user.setUseremail(testUseremail);
		assertEquals(testUseremail, user.getUseremail());
	}

	public void testSetAndGetPassword(){
		String testPassword ="qwerasdf";
		assertNull(user.getPassword());
		user.setPassword(testPassword);
		assertEquals(testPassword, user.getPassword());
	}
	
	public void testSetAndGetPhone(){
		String testPhone ="12345";
		assertNull(user.getUserphone());
		user.setUserphone(testPhone);
		assertEquals(testPhone, user.getUserphone());
	}
	
	public void testSetAndGetName(){
		String testName ="iii";
		assertNull(user.getUsername());
		user.setUsername(testName);
		assertEquals(testName, user.getUsername());
	}


}
