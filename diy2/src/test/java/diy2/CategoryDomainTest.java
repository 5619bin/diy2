package diy2;

import au.edu.sydney.domain.Category;
import junit.framework.TestCase;

public class CategoryDomainTest extends TestCase {
	
	private Category category;
	
	@Override
	protected void setUp() throws Exception {
		// TODO Auto-generated method stub
		category =new Category();
	}
	
	
	public void testSetAndGetName(){
		String testName ="aName";
		assertNull(category.getCname());
		category.setCname(testName);
		assertEquals(testName, category.getCname());
	}

}
