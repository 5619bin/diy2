package diy2;

import java.sql.Timestamp;

import au.edu.sydney.domain.Orders;
import junit.framework.Test;
import junit.framework.TestResult;

import junit.framework.TestCase;

public class OrderDomainTest extends TestCase {
	
	private Orders order;

	@Override
	protected void setUp() throws Exception {
		// TODO Auto-generated method stub
		order =new Orders();
	}
	
	public void testSetAndGetDescription(){
		String testDescription ="aDescription";
		assertNull(order.getDescription());
		order.setDescription(testDescription);
		assertEquals(testDescription, order.getDescription());
	}

	public void testSetAndGetPrice() {
		double testPrice = 100.00;
		assertEquals(0,0,0);
		order.setTotal_price(testPrice);
		assertEquals(testPrice, order.getTotal_price(), 0);
	}
	

	public void testSetAndGetOrder_status(){
		String testOrder_status ="test status";
		assertNull(order.getOrder_status());
		order.setOrder_status(testOrder_status);
		assertEquals(testOrder_status, order.getOrder_status());
	}
	public void testSetAndGetOrder_time(){
		Timestamp testOrder_tim =new Timestamp(System.currentTimeMillis());
		assertNull(order.getOrder_time());
		order.setOrder_time(testOrder_tim);
		assertEquals(testOrder_tim, order.getOrder_time());
	}
	public void testSetAndGetUser_visiable(){
		int testUser_visiable =1;
		assertNull(order.getUser_visiable());
		order.setUser_visiable(testUser_visiable);
		assertEquals(testUser_visiable, order.getUser_visiable());
	}
	
}
