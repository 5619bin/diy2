package diy2;

import org.junit.Test;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import au.edu.sydney.dao.CategoryDao;
import au.edu.sydney.domain.Category;
import junit.framework.Assert;

public class TestCategoryDao {
	
	@Autowired
    private CategoryDao categoryDao;
	
	@Test
    @Transactional
    @Rollback(true)
    public void testSaveCategory()
    {
        Category c= new Category();
        c.setCnum(14);
        c.setCname("teeest");
        categoryDao.saveCategory(c);
         
        List<Category> categories = categoryDao.listcategory();
        Assert.assertEquals(c.getCname(), categories.get(14).getCname());
    }

}
