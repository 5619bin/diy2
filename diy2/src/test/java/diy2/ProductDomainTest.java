package diy2;

import au.edu.sydney.domain.Product;
import junit.framework.Test;
import junit.framework.TestResult;

import junit.framework.TestCase;

public class ProductDomainTest extends TestCase {
	
	private Product product;

	@Override
	protected void setUp() throws Exception {
		// TODO Auto-generated method stub
		product =new Product();
	}
	
	public void testSetAndGetDescription(){
		String testDescription ="aDescription";
		assertNull(product.getDescription());
		product.setDescription(testDescription);
		assertEquals(testDescription, product.getDescription());
	}
	
	public void testSetAndGetName(){
		String testName ="aName";
		assertNull(product.getPname());
		product.setPname(testName);
		assertEquals(testName, product.getPname());
	}
	
	public void testSetAndGetQuantity(){
		int testQuantity = 100;
		assertEquals(0,0,0);
		product.setQuantity(testQuantity);
		assertEquals(testQuantity, product.getQuantity(), 0);
	}

	
	public void testSetAndGetPrice() {
		double testPrice = 100.00;
		assertEquals(0,0,0);
		product.setPrice(testPrice);
		assertEquals(testPrice, product.getPrice(), 0);
	}
	

}
