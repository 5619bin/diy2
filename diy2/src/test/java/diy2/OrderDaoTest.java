package diy2;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import junit.framework.TestCase;
import au.edu.sydney.dao.OrderDao;
import au.edu.sydney.domain.Orders;

public class OrderDaoTest extends TestCase {

	private OrderDao orderDao;
	
	private List<Orders> orders;
	private Orders order;
	
	
	@Override
	protected void setUp() throws Exception {
		// TODO Auto-generated method stub
		orderDao= new OrderDao();
		orders =new ArrayList<Orders>();
		order=new Orders();
		
		//stub up a list of products
		order.setUser_id(999);
		order.setOrder_time(new Timestamp(System.currentTimeMillis()));
		order.setPick_time(new Timestamp(System.currentTimeMillis()));
		order.setOrder_status("paid");
		order.setTotal_price(30.00);
		order.setDescription("this is first order");
		order.setUser_visiable(0);
		//orderDao.saveOrder(order);
		orders.add(order);
	
	}
	
	public void testGetProductsWithNoOrders(){
		orderDao = new OrderDao();
		assertNull(orders.get(0));
	}

	public void testGetOrders(){
		//List<Orders> theOrders = orderDao.getOrder();
	//	assertNotNull(theOrders);
		assertEquals(30.00, orders.get(0).getTotal_price());
		
		Orders order = orders.get(0);
		assertEquals("this is first order", order.getDescription());
		assertEquals("paid", order.getOrder_status());
		

	}
	
}
