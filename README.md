# My project's README

DIY order is a online ordering system based on a fast-food picking up restaurant. 

 The features of “customer” part involved with this project are as follows:
	1.	Management of user information
	2.	Order online
	3.	Management of history recording
	
And the part of “company” will include following features:
	1.	Management of product
	2.	Management of orders
	3.  Management of Category
	4.  Refresh the orders
	
Main function:	

1.By Shuang Zhang
Management of user information
This part is used by customer, it includes the user sign up and sign up.
One user name only can sign up once time. And each attribute must be input.
And user can through it to look the information of themselves and can change this information. 
The password can be changed too.

2.By Bin Yang
Order online
Customer can use web application to order the food before they go to take the food. 
The order online divides into two methods, set meal and DIY order.
Set meal combined with some settled product. And the DIY order customer can select any vegetable or meet or other product by themselves. 
The price of setmeal is settled and the price of the DIY order will according to which product they choose. 
Customer also can choose the time to take the food. After selected the food they need to pay the bill online.

3.By Shuang Zhang
Management of history recording
Customer and Company
Customer and company can look their history of orders. 
There will show all the order’s detail and customer can delete the history or search some special order by the time or order number.
The different is customer only can see their own orders but the company can see everyone’s orders. And the company cannot cancel any order.

4.By Lang Deng
Management of category
Company
The company can through the web to look the category of they already have and they can add to change or delete this category.

5.By Lang Deng
Management of product
Company
The company can through the web to look the product of they already have and they can add to change or delete this product.

6.By Lang Deng
Management of inventory
Company
With the web application, the company can change inventory of product.

7.By Shuang Zhang
Company Log in
Company sign in function. Name is "admin" and password is "admin" too.